use std::fs;
use clap::{Arg, App};
use regex::Regex;
use std::vec::Vec;

fn count(data_string: &str, bits: usize) -> Result<Vec<i32>, Box<dyn std::error::Error>> {
    let mut regex : String = "".to_string();
    for _ in 0..bits {
        regex += "(0|1)";
    }

    let re = Regex::new(regex.as_str())?;
    let mut counts = vec![0; bits.into()];
    for captures in re.captures_iter(&data_string) {
        for i in 0..bits {
            counts[i as usize] += match &captures[i as usize + 1] {
                "0" => -1,
                "1" => 1,
                _   => 0
            };
        }
    }

    Ok(counts)
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let matches = App::new("Day 3")
        .version("1.0.0")
        .author("Julian Santos <julian.f.santos88@gmail.com")
        .about("Submarine Diagnostics")
        .arg(Arg::with_name("file")
                .short("f")
                .long("file")
                .takes_value(true)
                .help("Diagnostic Report"))
        .get_matches();

    let file = matches.value_of("file").unwrap_or("inputs/day3.txt");

    let file_string = fs::read_to_string(file)?;
    let amount : usize = 12;
    let counts = count(&file_string, amount)?;

    println!("{:?}", counts);

    let mut gamma_rate : u32 = 0;
    let mut epsilon_rate : u32 = 0;
    let mut exp : i32 = amount as i32 - 1;
    for value in counts.iter() {
        let gamma_coef = if *value > 0 { 1 } else { 0 };
        let epsilon_coef = if *value > 0 { 0 } else { 1 };
        gamma_rate += gamma_coef * u32::pow(2, exp as u32);
        epsilon_rate += epsilon_coef * u32::pow(2, exp as u32);
        exp -= 1;
    }

    println!("Gamma Rate: {}, Epsilon Rate: {}, Answer: {}", gamma_rate, epsilon_rate, gamma_rate * epsilon_rate);


    let mut oxygen_generator_rating = 0;
    let mut co2_scrubber_rating = 0;
    let mut found_oxy = false;
    let mut found_co2 = false;
    let mut oxy_string = file_string.clone();
    let mut co2_string = file_string.clone();
    let mut oxy_regex_string = "(?m)^(".to_string();
    let mut co2_regex_string = "(?m)^(".to_string();
    let mut index = 0;
    while (!found_oxy || !found_co2) && index < amount {
        let oxy_counts = count(&oxy_string, amount)?;
        oxy_regex_string += if oxy_counts[index] >= 0 { "1" } else { "0" };
        let oxy_temp_regex = oxy_regex_string.clone() + ").*$";
        let oxy_regex = Regex::new(&oxy_temp_regex)?;
        let mut next_oxy_string : String = "".to_string();
        let mut next_oxy_captures = 0;
        for capture in oxy_regex.captures_iter(&oxy_string) {
            next_oxy_string += &((&capture[0]).to_string() + "\n");
            next_oxy_captures += 1;
        }
        oxy_string = next_oxy_string;

        if next_oxy_captures == 1 {
            oxygen_generator_rating = isize::from_str_radix(&oxy_string[0..(oxy_string.len() - 2)], 2).unwrap();
            found_oxy = true;
        }

        let co2_counts = count(&co2_string, amount)?;
        co2_regex_string += if co2_counts[index] >= 0 { "0" } else { "1" };
        let co2_temp_regex = co2_regex_string.clone() + ").*$";
        let co2_regex = Regex::new(&co2_temp_regex)?;
        let mut next_co2_string : String = "".to_string();
        let mut next_co2_captures = 0;
        for capture in co2_regex.captures_iter(&co2_string) {
            next_co2_string += &((&capture[0]).to_string() + "\n");
            next_co2_captures += 1;
        }
        co2_string = next_co2_string;

        if next_co2_captures == 1 {
            co2_scrubber_rating = isize::from_str_radix(&co2_string[0..(co2_string.len() - 2)], 2).unwrap();
            found_co2 = true;
        }

        index += 1;
    }

    println!("Oxygen: {}, CO2: {}, Answer: {}", oxygen_generator_rating, co2_scrubber_rating, oxygen_generator_rating * co2_scrubber_rating);

    Ok(())
}