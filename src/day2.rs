use std::fs;
use clap::{Arg, App};
use regex::Regex;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let matches = App::new("Day 2")
        .version("1.0.0")
        .author("Julian Santos <julian.f.santos88@gmail.com")
        .about("Submarine Depths")
        .arg(Arg::with_name("file")
                .short("f")
                .long("file")
                .takes_value(true)
                .help("Submarine Commands"))
        .get_matches();

    let file = matches.value_of("file").unwrap_or("inputs/day2.txt");

    let file_string = fs::read_to_string(file)?;

    let mut horizontal_pos : u32 = 0;
    let mut depth : i32 = 0;
    let re = Regex::new(r"(forward|down|up)\s([0-9]+)")?;
    for captures in re.captures_iter(&file_string) {
        match &captures[1] {
            "forward" =>    horizontal_pos += &captures[2].parse::<u32>().unwrap(),
            "up" =>         depth -= &captures[2].parse::<i32>().unwrap(),
            "down" =>       depth += &captures[2].parse::<i32>().unwrap(),
            _ =>            ()
        }
    }

    println!("Horizontal: {}, Depth: {}, Answer: {}", horizontal_pos, depth, horizontal_pos as i32 * depth);

    let mut aim : i64 = 0;
    let mut horizontal_pos : i64 = 0;
    let mut depth : i64 = 0;
    for captures in re.captures_iter(&file_string) {
        match &captures[1] {
            "forward" =>    {
                            horizontal_pos += &captures[2].parse::<i64>().unwrap();
                            depth += aim * &captures[2].parse::<i64>().unwrap();
            },
            "up" =>         aim -= &captures[2].parse::<i64>().unwrap(),
            "down" =>       aim += &captures[2].parse::<i64>().unwrap(),
            _ =>            ()
        }
    }

    println!("Horizontal: {}, Depth: {}, Answer: {}", horizontal_pos, depth, horizontal_pos * depth);

    Ok(())
}