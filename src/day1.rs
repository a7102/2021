use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use clap::{Arg, App};
use std::collections::VecDeque;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let matches = App::new("Day 1")
        .version("1.0.0")
        .author("Julian Santos <julian.f.santos88@gmail.com")
        .about("Submarine Depths")
        .arg(Arg::with_name("file")
                .short("f")
                .long("file")
                .takes_value(true)
                .help("The file with sweep depths"))
        .get_matches();

    let file = matches.value_of("file").unwrap_or("inputs/day1.txt");

    let file_reader = match File::open(file) {
        Ok(file) => BufReader::new(file),
        Err(e) => return Err(Box::new(e))
    };

    let lines = file_reader.lines();
    let mut last_line : u32 = 0;
    let mut first = true;
    let mut increases : u32 = 0;
    for line in lines {
        if let Ok(line) = line {
            let line_value = line.parse::<u32>().unwrap();
            if first {
                println!("{} (N/A - no previous measurement)", line);
                first = false;
            } else {
                if line_value > last_line {
                    increases += 1;
                }
            }


            last_line = line_value;
        }
    }

    println!("{} increases!", increases);

    let file_reader = match File::open(file) {
        Ok(file) => BufReader::new(file),
        Err(e) => return Err(Box::new(e))
    };

    increases = 0;
    let lines = file_reader.lines();
    let mut deq : VecDeque<i32> = VecDeque::new();
    for line in lines {
        if let Ok(line) = line {
            let line_value = line.parse::<i32>().unwrap();
            deq.push_back(line_value);
            if deq.len() > 4 {
                deq.pop_front();
            }

            if deq.len() == 4 {
                let diff = deq[3] - deq[0];
                if diff > 0 {
                    increases += 1;
                }
            }
        }
    }

    println!("{} slide increases!", increases);


    Ok(())
}
