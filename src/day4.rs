use std::fs;
use clap::{Arg, App};
use regex::Regex;
use std::thread;
use std::sync::{Arc, Mutex};
use std::sync::atomic::{AtomicBool, Ordering};

#[cfg(windows)]
const LINE_ENDING: &'static str = "\r\n";
#[cfg(windows)]
const BOARD_ENDING: &'static str = "\r\n\r\n";
#[cfg(not(windows))]
const LINE_ENDING: &'static str = "\n";
#[cfg(not(windows))]
const BOARD_ENDING: &'static str = "\n\n";

fn update_board(board: &Vec<Vec<(i16, bool)>>, drawn_number: i16) -> Result<Vec<Vec<(i16, bool)>>, Box<dyn std::error::Error>> {
    let mut out_board = board.to_vec();
    struct Location {
        col: usize,
        row: usize
    }
    let mut pos = Location { col: 0, row: 0 };
    for col in board {
        for row in col {
            if row.0 == drawn_number {
                out_board[pos.col][pos.row] = (row.0, true);
                return Ok(out_board)
            }
            pos.row += 1;
        }
        pos.row = 0;
        pos.col += 1;
    }
    return Ok(out_board)
}

fn check_row(row: &Vec<(i16, bool)>) -> bool {
    let mut count = 0;
    for item in row {
        if item.1 == true { count += 1 }
    }

    count == 5
}

fn check_columns(array: &Vec<Vec<(i16, bool)>>) -> bool {
    for col in 0..5 {
        let mut count = 0;
        let _result: Vec<()> = array.iter().map(|x| if x[col].1 == true { count += 1; } ).collect();
        if count == 5 { 
            return true 
        }
    }

    false
}

#[allow(dead_code)]
fn check_crosses(array: &Vec<Vec<(i16, bool)>>) -> bool {
    if array[0][0].1 && array[1][1].1 && array[2][2].1 && array[3][3].1 && array[4][4].1 {
        return true
    }

    if array[0][4].1 && array[1][3].1 && array[2][2].1 && array[3][1].1 && array[4][0].1 {
        return true
    }    

    false
}

fn check_for_winner(board: &Vec<Vec<(i16, bool)>>, winner: &AtomicBool, stop: bool) -> Result<(Vec<Vec<(i16, bool)>>, bool), Box<dyn std::error::Error>> {
    let error_type = Box::new(std::io::Error::new(std::io::ErrorKind::InvalidData, "Not Winner"));
    for row in board {
        if winner.load(Ordering::Relaxed) && stop { return Err(error_type) }
        let win = check_row(row);
        if win {
            winner.store(true, Ordering::Relaxed);
            return Ok((board.to_vec(), true))
        }
    }

    if winner.load(Ordering::Relaxed) && stop { return Err(error_type) }
    let win = check_columns(board);
    if win {
        winner.store(true, Ordering::Relaxed);
        return Ok((board.to_vec(), true))
    }

    // if winner.load(Ordering::Relaxed) { return Err(error_type) }
    // let win = check_crosses(board);
    // if win {
    //     println!("won");
    //     winner.store(true, Ordering::Relaxed);
    //     return Ok(board.to_vec())
    // }

    Err(error_type)
}

fn get_board_score(board: &Vec<Vec<(i16, bool)>>) -> i32 {
    let mut score = 0;
    for row in board {
        for col in row {
            if !col.1 { score += col.0 as i32 } 
        }
    }

    score
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let matches = App::new("Day 4")
        .version("1.0.0")
        .author("Julian Santos <julian.f.santos88@gmail.com")
        .about("Giant Squid")
        .arg(Arg::with_name("file")
                .short("f")
                .long("file")
                .takes_value(true)
                .help("Bingo Boards"))
        .get_matches();

    let file = matches.value_of("file").unwrap_or("inputs/day4.txt");
    let file_string = fs::read_to_string(file)?;

    let re = Regex::new(r"([0-9,]+)\s+([0-9 \s]*)")?;
    let captures = re.captures(&file_string).unwrap();
    let drawn : String = captures[1].to_string();
    let string_boards : String = captures[2].to_string();

    let instructions : Vec<&str> = drawn.split(",").collect();
    let mut numeric_instructions = vec!();
    for instruction in instructions {
        numeric_instructions.push(instruction.parse::<i16>().unwrap());
    }

    let all_string_boards = string_boards.split(BOARD_ENDING);

    let mut boards = vec!();
    let value_splitter = Regex::new(r"([0-9]+)\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)\s+([0-9]+)")?;
    for string_board in all_string_boards {
        let mut line_index = 0;
        let mut board = vec![vec![(0, false); 5]; 5];
        for line in string_board.split(LINE_ENDING) {
            for capture in value_splitter.captures_iter(line) {
                board[line_index][0] = ((&capture[1]).parse::<i16>().unwrap(), false);
                board[line_index][1] = ((&capture[2]).parse::<i16>().unwrap(), false);
                board[line_index][2] = ((&capture[3]).parse::<i16>().unwrap(), false);
                board[line_index][3] = ((&capture[4]).parse::<i16>().unwrap(), false);
                board[line_index][4] = ((&capture[5]).parse::<i16>().unwrap(), false);
            }
            line_index += 1;
        }
        boards.push(Arc::new(Mutex::new((board, false))));
    }

    let winner = Arc::new(AtomicBool::new(false));
    let winning_board : (Vec<Vec<(i16, bool)>>, bool) = (vec!(), false);
    let arc_winning_board = Arc::new(Mutex::new(winning_board));
    let mut instruction_index = 0;
    let mut first_last_instruction = -1;
    let mut first_board_score = 0;
    let mut last_last_instruction = -1;
    let mut last_board_score = 0;
    let mut stop = true;
    for instruction in numeric_instructions {
        let mut thread_vec = vec!();
        for i in 0..boards.len() {
            let arc_board = Arc::clone(&boards[i]);
            let handle = thread::spawn(move || {
                let mut array = arc_board.lock().unwrap();
                *array = match update_board(&array.0, instruction) {
                    Ok(result) => (result, array.1),
                    _ => ((&array.0).to_vec(), array.1)
                };
            });
            thread_vec.push(handle);
        }

        while let Some(current_thread) = thread_vec.pop() {
            current_thread.join().unwrap();
        }

        if instruction_index > 4 {
            for i in 0..boards.len() {
                let arc_board = Arc::clone(&boards[i]);

                let arc_winner = Arc::clone(&winner);
                let arc_win_board = Arc::clone(&arc_winning_board);
                let handle = thread::spawn(move || {
                    let mut array = arc_board.lock().unwrap();
                    if array.1 {
                        return;
                    }
                    match check_for_winner(&array.0, &arc_winner, stop) {
                        Ok(result) => {
                            let mut board = arc_win_board.lock().unwrap();
                            *board = result.clone();
                            *array = result.clone();
                            println!("Won {}", i);
                        },
                        Err(_) => ()
                    }
                });
                thread_vec.push(handle);
            }

            while let Some(current_thread) = thread_vec.pop() {
                current_thread.join().unwrap();
            }
        }

        instruction_index += 1;
        println!("Finished Round {}, {}", instruction_index, instruction);
        if winner.load(Ordering::Relaxed) && first_last_instruction == -1 {
            first_board_score = get_board_score(&arc_winning_board.lock().unwrap().0);
            first_last_instruction = instruction;
            stop = false;
            winner.store(false, Ordering::Relaxed);
        } else if winner.load(Ordering::Relaxed) {
            last_board_score = get_board_score(&arc_winning_board.lock().unwrap().0);
            last_last_instruction = instruction;
            winner.store(false, Ordering::Relaxed);
        }
    }

    let first_final_score = first_last_instruction as i32 * first_board_score;
    println!("Final Score {} = {} * {}", first_final_score, first_board_score, first_last_instruction);
    let last_final_score = last_last_instruction as i32 * last_board_score;
    println!("Final Score {} = {} * {}", last_final_score, last_board_score, last_last_instruction);

    Ok(())
}